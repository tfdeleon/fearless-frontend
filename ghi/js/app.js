function createCard(name, description, pictureUrl,endDate,startDate,location) {
  return `
  <div class='col-4'>
    <div class="card h-85 shadow p-3 mb-5">
      <img src= ${pictureUrl} class="card-img-top" alt='...'>
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted"> ${location} </h6>
        <p class="card-text">${description}</p>
        <div class="card-footer">
          ${startDate} - ${endDate}
      </div>
    </div>
  `;
}
window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = (new Date(details.conference.starts).toLocaleDateString("en-us"))
          const endDate = (new Date( details.conference.ends).toLocaleDateString("en-us"))
          const html = createCard(title, description, pictureUrl, startDate, endDate);
          const column = document.querySelector('.row');
          column.innerHTML += html

        }
      }

    }
  } catch (e) {
    // Figure out what to do if an error is raised
  }

});
