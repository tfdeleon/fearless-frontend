// Get the cookie out of the cookie store
const payloadCookie = getCookie('payloadCookie'); // Replace 'payloadCookie' with the actual name of the cookie

if (payloadCookie) {
  // The cookie value is a JSON-formatted string, so parse it
  const encodedPayload = JSON.parse(payloadCookie.value);

  // Convert the encoded payload from base64 to a normal string
  const decodedPayload = atob(encodedPayload);

  // The payload is a JSON-formatted string, so parse it
  const payload = JSON.parse(decodedPayload);

  // Print the payload
  console.log(payload);

  // Check if "events.add_conference" is in the permissions.
  // If it is, remove 'd-none' from the conference link
  if (payload.permissions.includes('events.add_conference')) {
    // Remove 'd-none' from the conference link
    const conferenceLink = document.querySelector('.nav-item[data-link="conference"] a');
    conferenceLink.classList.remove('d-none');
  }

  // Check if "events.add_location" is in the permissions.
  // If it is, remove 'd-none' from the location link
  if (payload.permissions.includes('events.add_location')) {
    // Remove 'd-none' from the location link
    const locationLink = document.querySelector('.nav-item[data-link="location"] a');
    locationLink.classList.remove('d-none');
  }
}

// Helper function to get the value of a cookie by name
function getCookie(cookieName) {
  const cookies = document.cookie.split('; ');
  for (let i = 0; i < cookies.length; i++) {
    const [name, value] = cookies[i].split('=');
    if (name === cookieName) {
      return { name, value };
    }
  }
  return null;
}
