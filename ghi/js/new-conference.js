window.addEventListener('DOMContentLoaded', async () => {

    // State fetching code, here...
    const url = "http://localhost:8000/api/locations/"
    try{
        const response = await fetch(url)
        if(!response.ok){
            throw new Error(" ");
        }else{
            const data = await response.json();
            const selectTag = document.getElementById('location');
            for(let location of data.location){
                const option = document.createElement('option');
                option.value = location.name;
                option.innerHTML = location.name;
                selectTag.appendChild(option);
            }
            const formTag = document.getElementById("create-conference-form");
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));
                const conferenceUrl = "http://localhost:8000/api/conferences/";
                const fetchConfig = {
                    method: "POST",
                    body: json,
                    headers:{
                        "Content-Type": "application/json",
                    },
                };
                const response = await fetch(conferenceUrl,fetchConfig);
                if (response.ok){
                    formTag.reset();
                    const newLocation = await response.json()
                }
            })
        }
    } catch(e){
        console.error(e);
    }
  });
