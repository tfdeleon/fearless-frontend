// import './App.css';
import React from "react";
import Nav from "./Nav";
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from "./ConferenceForm";
import PresentationForm from "./PresentationForm"
import AttendeeConferenceForm from './attend-conference';
import MainPage from "./MainPage";
import { BrowserRouter, Route, Routes } from 'react-router-dom';


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="/conference/new" element={<ConferenceForm/>}/>
          <Route path="/attendees/new" element={<AttendeeConferenceForm/>}/>
          <Route path="/locations/new" element={<LocationForm/>}/>
          <Route path="/conferences/new" element={<AttendeesList/>}/>
          <Route path="/presentation/new" element={<PresentationForm/>}/>
          <Route path="/attendees" element={<AttendeeConferenceForm attendees={props.attendees}/>}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}
export default App;
