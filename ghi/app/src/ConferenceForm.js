import React, {useEffect, useState} from "react";

function ConferenceForm(){
    const [name, setName] = useState('');
    const [start, setStart] = useState('');
    const [end, setEnd] = useState('');
    const [description, setDescription] = useState('');
    const [maxPresentations, setMaxPresentations] = useState('');
    const [maxAttendees, setMaxAttendees] = useState('');
    const [location, setLocation] = useState('');
    const [locations, setLocations] = useState([]);
    //handlers
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleStartChange = (event) => {
        const value = event.target.value;
        setStart(value);
    }
    const handleEndChange = (event) => {
        const value = event.target.value;
        setEnd(value);
    }
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }
    const handleMaxPresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }
    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }
    const handleLocationChange = (event) => {
        const selectedIndex = event.target.selectedIndex;
        const locationId = event.target.options[selectedIndex].value;
        setLocation(locationId);
        console.log(event.target.options)
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);

        if(response.ok){
            const data = await response.json();
            setLocations(data.locations);
            console.log(data.locations)
        }
    }


    const handleSubmit = async (event) =>{
        event.preventDefault();
        const data = {};
        data.name = name;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.starts = start;
        data.ends = end;
        data.location = location;
        console.log(data)
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);

            if(response.ok) {
                const newConference = await response.json();
                console.log(newConference);
                setName('');
                setStart('');
                setEnd('');
                setDescription('');
                setMaxPresentations('');
                setMaxAttendees('');
                setLocation('')
            }
    }



    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new conference</h1>
              <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                  <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={name} />
                  <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleStartChange} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" value={start} />
                  <label htmlFor="starts">Starts</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleEndChange} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" value={end} />
                  <label htmlFor="ends">Ends</label>
                </div>
                <div className="mb-3">
                  <label htmlFor="description" className="form-label">Description</label>
                  <textarea onChange={handleDescriptionChange} required type="text" name="description" id="description" className="form-control" value={description}></textarea>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleMaxPresentationsChange} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" value={maxPresentations}/>
                  <label htmlFor="max_presentations">Maximum presentations</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleMaxAttendeesChange} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" value={maxAttendees}/>
                  <label htmlFor="max_attendees">Maximum attendees</label>
                </div>
                <div className="mb-3">
                  <select onChange={handleLocationChange} required id="location" name="location" className="form-select" value={location}>
                    <option value=''>Choose a location</option>
                    {locations.map((location) =>(
                        <option key={location.id} value={location.id}>{location.name}</option>
                    ))}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm
